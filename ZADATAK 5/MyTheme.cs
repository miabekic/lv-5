﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_5
{
    class MyTheme:ITheme
    {
        public void SetBackgroundColor()
        {
            Console.BackgroundColor = ConsoleColor.Blue;
        }
        public void SetFontColor()
        {
            Console.ForegroundColor = ConsoleColor.White;
        }
        public string GetHeader(int width)
        {
            return new string('*', width);
        }
        public string GetFooter(int width)
        {
            return new string('_', width);
        }
    }
}
