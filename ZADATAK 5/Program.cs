﻿using System;

namespace ZADATAK_5
{
    class Program
    {
        static void Main(string[] args)
        {
            ITheme theme = new LightTheme();
            ITheme myTheme = new MyTheme();
            Note note = new ReminderNote("Napisi zadacu!", theme);
            Note note1 = new ReminderNote("Popi vode svakih sat vremena!", myTheme);
            note.Show();
            note1.Show();
        }
    }
}
