﻿using System;

namespace ZADATAK_3
{
    class Program
    {
        static void Main(string[] args)
        {
            DataConsolePrinter printer = new DataConsolePrinter();
            User user1 = User.GenerateUser("Mia");
            User user2 = User.GenerateUser("Nepoznati user");
            IDataset dataset = new VirtualProxyDataset(@"C:\Users\Mia\source\repos\LV-5\ZADATAK 3\CSV.txt");
            IDataset dataset1 = new ProtectionProxyDataset(user1);
            IDataset dataset2 = new ProtectionProxyDataset(user2);
            printer.PrintData(dataset);
            printer.PrintData(dataset1);
            printer.PrintData(dataset2);
        }
    }
}
