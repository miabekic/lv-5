﻿using System;

namespace ZADATAK_2
{
    class Program
    {
        static void Main(string[] args)
        {
            ShippingService service = new ShippingService(10.5);
            IShipable shipableItem = new Product("LCD televizor", 2300.75, 10.51);
            IShipable shipableItem1 = new Product("Laptop", 6471.20, 1.25);
            IShipable shipableItem2 = new Product("Slusalice", 50, 0.20);
            Console.WriteLine(shipableItem.Description() + "\n" + shipableItem.Weight + "kg\n" + shipableItem.Price + "kn\n" + "Cijena dostave: " + service.servicePrice(shipableItem) + "\n");
            Box box = new Box("Posiljka za Miu");
            box.Add(shipableItem1);
            box.Add(shipableItem2);
            Console.WriteLine(box.Description() + box.Weight + "kg\n" + box.Price + "kn\n" + "Cijena dostave: " + service.servicePrice(box) + "kn");
        }
    }
}
