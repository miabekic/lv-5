﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace ZADATAK_2
{
     class ShippingService
    {
        private double pricePerMass;
        public ShippingService(double pricePerMass)
        {
            this.pricePerMass = pricePerMass;
        }
        public double servicePrice(IShipable item)
        {
            double price = item.Weight * pricePerMass;
            return price;
        }
    }
}
