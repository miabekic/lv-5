﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_6
{
    class GroupNote:Note
    {
        private List<string> person;
        public GroupNote(string message, ITheme theme) : base(message, theme)
        {
            person = new List<string>();
        }
        public void AddPerson(string person)
        {
            this.person.Add(person);
        }
        public void RemovePerson(string person)
        {
            this.person.Remove(person);
        }
        public override void Show()
        {

            this.ChangeColor();
            Console.WriteLine("GROUPREMINDER: ");
            string framedMessage = this.GetFramedMessage();
            foreach (string student in person)
            {
                Console.WriteLine(student);
            }
            Console.WriteLine(framedMessage);
            Console.ResetColor();
        }
    }
}
