﻿using System;

namespace ZADATAK_6
{
    class Program
    {
        static void Main(string[] args)
        {
            ITheme theme = new LightTheme();
            ITheme myTheme = new MyTheme();
            GroupNote groupNote = new GroupNote("Izvjestaje predajete do subote u 15h", theme);
            GroupNote groupNote1 = new GroupNote("Nemojte olako shvatiti ovu situaciju!", myTheme);
            groupNote.AddPerson("Mia");
            groupNote.AddPerson("Tanja");
            groupNote.AddPerson("Sergej");
            groupNote1.AddPerson("Nenad");
            groupNote1.AddPerson("Davor");
            groupNote1.AddPerson("Sinisa");
            groupNote.Show();
            groupNote1.Show();
            groupNote.RemovePerson("Sergej");
            groupNote.Show();
            
        }
    }
}
