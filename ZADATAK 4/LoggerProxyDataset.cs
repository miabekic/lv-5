﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ZADATAK_4
{
    class LoggerProxyDataset:IDataset
    {
        private string filePath;
        private Dataset dataset;
        public LoggerProxyDataset(string filePath)
        {
            this.filePath = filePath;
        }
        public ReadOnlyCollection<List<string>> GetData()
        {
            ConsoleLogger logger = ConsoleLogger.GetInstance();
            logger.loggingTime();
            if (dataset == null)
            {
                dataset = new Dataset(filePath);
            }
            return dataset.GetData();
        }


    }
}
