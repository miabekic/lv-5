﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_4
{
    class DataConsolePrinter
    {
        public void PrintData(IDataset dataset)
        {
            IReadOnlyCollection<List<string>> data = dataset.GetData();
            foreach(List<string> Data in data)
            {
                foreach(string inSideData in Data)
                {
                    Console.Write(inSideData + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("\n");
        }
    }
}
