﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_4
{
    class ConsoleLogger
    {
        private static ConsoleLogger instance;
        private DateTime time;
        public static ConsoleLogger GetInstance()
        {
            if (instance == null)
                instance = new ConsoleLogger();
            return instance;
        }
        public void loggingTime()
        {
            time = DateTime.Now;
            Console.WriteLine("Vrijeme loggiranja: " + time);
        }
    }
}
