﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ZADATAK_4
{
    interface IDataset
    {
        ReadOnlyCollection<List<string>> GetData();
    }
}
