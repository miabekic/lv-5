﻿using System;

namespace ZADATAK_4
{
    class Program
    {
        static void Main(string[] args)
        {
            DataConsolePrinter printer = new DataConsolePrinter();
            IDataset dataset = new LoggerProxyDataset(@"C:\Users\Mia\source\repos\LV-5\ZADATAK 4\CSV.txt");
            printer.PrintData(dataset);
            printer.PrintData(dataset);
        }
    }
}
