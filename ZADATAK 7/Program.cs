﻿using System;

namespace ZADATAK_7
{
    class Program
    {
        static void Main(string[] args)
        {
            ITheme theme = new LightTheme();
            ITheme myTheme = new MyTheme();
            Notebook notebook = new Notebook();
            Notebook notebook1 = new Notebook(theme);
            GroupNote groupNote = new GroupNote("Izvjestaje predajete do subote u 15h", theme);
            GroupNote groupNote1 = new GroupNote("Nemojte olako shvatiti ovu situaciju!", myTheme);
            groupNote.AddPerson("Mia");
            groupNote.AddPerson("Tanja");
            groupNote.AddPerson("Sergej");
            groupNote1.AddPerson("Nenad");
            groupNote1.AddPerson("Davor");
            groupNote1.AddPerson("Sinisa");
            notebook.AddNote(groupNote);
            notebook.AddNote(groupNote1);
            notebook.ChangeTheme(myTheme);
            notebook.Display();
            notebook1.AddNote(groupNote);
            notebook1.AddNote(groupNote1);
            notebook1.Display();
        }
    }
}
